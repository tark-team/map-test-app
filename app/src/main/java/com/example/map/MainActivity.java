package com.example.map;

import android.location.Location;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import com.example.map.util.L;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.*;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MainActivity extends AppCompatActivity implements
        OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    /************************************
     * PUBLIC STATIC CONSTANTS
     ************************************/
    public static final LatLng LONDON_COORDINATES = new LatLng(51.5286416, -0.1015987);
    public static final int LOCATION_UPDATE_INTERVAL_TIME = 5000;
    public static final float MAP_ZOOM_COEFFICIENT = 17f;

    /*************************************
     * PRIVATE FIELDS
     *************************************/
    private static Location mLastLocation;
    private GoogleApiClient mGoogleApiClient;
    private SupportMapFragment mMapFragment;
    private Location mCurrentLocation;
    private GoogleMap mMap;

    /************************************
     * PROTECTED METHODS
     ************************************/
    protected synchronized void buildGoogleApiClient() {
        L.i("buildGoogleApiClient");
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        L.i("onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        L.plant(new L.DebugTree());
        mMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        buildGoogleApiClient();
    }

    @Override
    protected void onStart() {
        L.i("onStart");
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        L.i("onStop");
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    /************************************
     * PUBLIC METHODS
     ************************************/
    @Override
    public void onMapReady(GoogleMap map) {
        L.i("onMapReady");

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(getLocation())
                .zoom(MAP_ZOOM_COEFFICIENT)
                .build();

        CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
        map.moveCamera(cameraUpdate);
        map.addMarker(new MarkerOptions().position(cameraPosition.target).title("Marker"));

        mMap = map;

    }

    @Override
    public void onConnected(Bundle bundle) {
        L.i("onConnected");
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        mMapFragment.getMapAsync(this);
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {
        L.i("onConnectionSuspended");
        mMapFragment.getMapAsync(this);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        L.i("onConnectionFailed");
        mMapFragment.getMapAsync(this);
    }

    @Override
    public void onLocationChanged(Location location) {
        L.i("onLocationChanged", location);
        mCurrentLocation = location;
        moveCameraToLocation(location);
    }

    /************************************
     * PRIVATE METHODS
     ************************************/
    private LatLng getLocation() {
        L.i("getLocation");
        if (mLastLocation != null) {
            return new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
        } else {
            return LONDON_COORDINATES;
        }
    }

    private void startLocationUpdates() {
        L.i("startLocationUpdates");
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(LOCATION_UPDATE_INTERVAL_TIME);
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this
        );
    }

    private void moveCameraToLocation(Location location) {
        L.i("moveCameraToLocation", location);
        if (mMap == null) {
            return;
        }
        mMap.clear();
        mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(location.getLatitude(), location.getLongitude()))
                        .title("Marker")
        );
    }

}
